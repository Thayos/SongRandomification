﻿using Amazon;
using Amazon.DynamoDBv2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SongPicker.Utilities
{
    public class SPDataContext
    {
        private AmazonDynamoDBClient _Client;
        public SPDataContext()
        {
            AmazonDynamoDBConfig clientConfig = new AmazonDynamoDBConfig();
            clientConfig.RegionEndpoint = RegionEndpoint.USEast1;
            _Client = new AmazonDynamoDBClient(clientConfig);
        }

        public AmazonDynamoDBClient Client { get => _Client; }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon;
using Amazon.DynamoDBv2.Model;
using SongPicker.Utilities;

namespace SongPicker.Controllers
{
    [Route("api/[controller]")]
    public class SongListController : Controller
    {
        // GET api/SongList
        [HttpGet]
        public async Task<List<Document>> GetAsync()
        {
            AmazonDynamoDBClient client = new SPDataContext().Client;
            Table SongTable = Table.LoadTable(client, "SongList");

            ScanFilter scanFilter = new ScanFilter();
            scanFilter.AddCondition("Id", ScanOperator.IsNotNull);
            Search search = SongTable.Scan(scanFilter);

            return await search.GetRemainingAsync();
        }

        // GET api/SongList/5
        [HttpGet("{id}")]
        public async Task<Document> Get(int id)
        {
            AmazonDynamoDBClient client = new SPDataContext().Client;
            Table SongTable = Table.LoadTable(client, "SongList");

            return await SongTable.GetItemAsync(id);
        }

        // POST api/SongList
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/SongList/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/SongList/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
